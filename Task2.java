import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Task2{
	public static void main(String[] args) {
		LocalDateTime localDateTimeNow = LocalDateTime.now();
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("E MM dd HH:mm:ss");
		DateTimeFormatter yearFormatter = DateTimeFormatter.ofPattern("YYYY");

		String formattedDateTime = localDateTimeNow.format(dateTimeFormatter);	
		String formattedYear = localDateTimeNow.format(yearFormatter);	

		System.out.println("Tanggal sekarang adalah: " + formattedDateTime + " WIB " + formattedYear);
	}
}